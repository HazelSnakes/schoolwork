# TED

## How Reliable is Your Memory

Memories are not always completely true and can be altered. This talk details a case where a man was falsly accused of a crime and convicted based on a victim's false memory.
She explains that there were many cases of people going through psychotherapy that ended up with memories that they didn't have before and weren't true.
She ran multiple studies showing that they can implant new memories in people. These new memories were actually able to affect how people behaved, changing food preferences for instance.
She advocates against the psychotherapy techniques that alter memories.

## The Surprising Science of Happiness

Dan Gilbert starts by explaining that one of the adaptations that led to the human brain tripling in size quickly on the evolutionary scale was the prefrontal cortex.
The prefrontal cortex is able to simulate events in the mind to predict what would happen. However, it isn't always able to correctly judge how large of an effect events can have.
People are able to "synthesize" happiness. There are multiple examples of people bringing happiness out of bad situations. While most people assume that "synthesized happiness" isn't as fullfilling as the "natural" variety we have when we get what we want, that isn't necessarily the case.
The speaker aranged a study where people were asked to rank paintings then choose between the two middle-ranked ones. Afterwards, they were asked to rank the paintings again and the one they got was ranked higher than last time.
The experiment was repeated with people that experienced amnesia, to similar results. Freedom of choice helps with obtaining "natural happiness" but doesn't help with "synthetic happiness".
He performed a study on college students that showed that people were overall more satisfied with outcomes that they didn't have control over.

## Brain Magic

The presenter in this talk performs several slight of hand tricks on the audience. He explains that he is able to do these by decieving the brain to make it percieve things differently than how they are.
He does this by using tricks to divert our attention towards certain things and away from others. Our eyes are only able to fully percieve what we are directly focused on.
These tricks form the basis of almost all slight of hand acts. Slight of hand is interesting because it shows how easy it is to fool the human mind. 

## The Happy Secret To Better Work

A major assumption in our society is that hard work and more success means more happiness. However, this isn't necessarily the case and it can sometimes be the opposite.
When you take into account all of the external factors affecting someone, you can only gauge about 1/10th of their long term happiness.
Being able to find or make happiness with what is happening in the present, can help increase work efficiency and quality.
The school and work environments don't do a very good job, however, of supporting this idea.
If someone is only able to focus on getting work done and succeeding more repeatedly, they won't ever be able to reach full satisfaction.


## The New Era Of Positive Psychology

At the beginning of the talk Martin describes the current state of psychology as "not good enough". He also says that he is hopeful that it will be good enough in coming years.
The science of mental illness has grown greatly in the last 50 years, being able to actually come up with treatments and experiments on mental disorders with the "disease model".
However, there are some negatives to the disease model, such as people who needed treatment being thought of as vicims of circumstance and a lack of focus on improving average people's lives as well.
Positive psychology deals with building strength as well as fixing what's broken. It also works on making the lives of normal people better.
He notes that experiencing as many pleasures as possible doesn't make someone as happy in the long run.
