# Vertigo

The movie follows a police detective named Scottie who has an extreme fear of heights after witnessing a fellow officer fall to his death trying to save him.
He is tasked with keeping an eye on a friends wife, Madeline and begins to see that she is mentally unstable. He and Madeline end up falling in love, but Madeline ends up taking her
own life by jumping out of a bell tower, which Scottie couldn't follow her up because of his vertigo.
Some time later he meets a woman who reminds him a lot of Madeline, Judy, and the two fall in love with each other. He obsessively makes her dress and act like like the original Madeline, and
she goes with it because of how much she loves him. He leads her to the bell tower to reinact Madeline's final moments and even overcomes his fear of heights in his frenzy.
At the top of the tower he realizes that Judy was the Madeline he knew, and was an accomplice in his friend's plot to murder his wife.
They make come to forgiveness, but Judy is startled by a nun and falls out of the tower to her death.

One psychological aspect of this movie is Scottie's vertigo, fear of heights. His vertigo in particular stems from his traumatic experience, phobias caused by trauma are known to be
more potent than those of people who haven't experienced what they have a phobia of. Another important thing is Scottie's obsession with dressing Judy up and turning her into  Madeline.
He has a delusional love of Madeline, trying to resurect her with Judy. He doesn't consider or think of the person Judy really is.

I am a huge fan of Alfred Hitchcock's work and Vertigo is no exception. The music and visuals are, as usual, very excellent at portraying the mood and themes of the movie.
Though I would still rank Rear Window and The Birds higher than it in personal preference. This movie ties very deep into human psychology and specifically trauma and obsession.
I would highly reccommend watching it.
