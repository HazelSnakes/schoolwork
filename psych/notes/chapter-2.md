# Neurons, The Nervous System, and the Endocrine System

![Neuron](https://upload.wikimedia.org/wikipedia/commons/b/b5/Neuron.svg)

- Denrites - Recieve messages from other cells
- Cell Body - The cell's life support center
- Axon - Passes messages away from the cell body to other neurons, muscles or glands
- Myelin Sheath - Covers the axon of some neurons and helps speed neural impulses
- Terminals - Form junctions with other cells

Neurons do *not* touch each other; the space in between them is called **the synapse**.

(Each neuron is connected to between 1,000 and 10,000 others.)

### How a Neuron Fires
It is an **electrochemical** process:
- Electrical inside the neuron
- Chemical outside the neuron (in the synapse in the form of a neurotransmitter)
- The fireing is called **Action Potential**

### Steps of Action Potential
1. Dendrites receive neurotransmitter from another neuron across the synapse.
2. Reached its **threshold**, then fires based on the all-or-none response.
3. Opens up a portal in axon, and lets in positive ions (Sodium) which mix with negative ions (Potassium) that is already inside the axon (thus Neurons at rest have a slightly negative charge).
4. The mixing of positive and negative ions causes an electrical charge that opens up the next portal (letting in more potasium) while closing the original portal.
5. Process continues down axon to the axon terminal.
6. Terminal buttons turns electrical charge into chemical (neurotransmitter) and shoots message to next neuron across the synapse.

## Neurotransmitters
Chemical messengers released by terminal buttons through the synapse.
- We should know at least 6 types and what they do.

### Acetylcholine
Its function is *motor movement* and possible memory.

- Too much leads to restlessness.
- Too little leads to lethargy.
- Lack of ACH has been linked to Alzheimer's disease.

### Dopamine
It helps conrol the brain's reward and pleasure centers, movement, and emotional responses.

- Not only allows us to see rewards, but to take action to move toward them.
- Lack of dopamine is associated with Parkinson's disease.
- Overabundance is associated with schizophrenia.

### Serotonin
Function deals with mood control, sleep, and hunger.

- Lack of serotonin has been linked to depression.

### Endorphins
Function deals with pain control.

- We become addicted to endorphin-causing feelings.

### GABA
An **inhibitory** neurotransmitter

- Does *not* stimulate the brain - balances out excitatory neurotransmitters.
- Too little GABA can lead to insomnia or seizures.

### Glutamine
And excitatory neurotransmitter

- Would stimulate the brain.
- Too much can also lead to seizures or migraines.

### What are agonists and antagonists?
They are drugs
- **Agonists** mimic neruotransmitters.
    - Ex: Nicotine is an ACh agonist
- **Antagonists** block neurotransmitters.
    - Ex: Curare is and antagonist for ACh (paralyzes you)

