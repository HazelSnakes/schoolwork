# Chapter 6 - Perception

Perception occurs when the brain organizes, analyzes, and integrates sensory information. It is as if the brain is putting together pieces of a puzzle.

Our perceptions of the world are influenced by bottom up and top down processing.

### Top Down & Bottom Up Processing
- The gathering of data by the senses is considered **bottum up processing**.
- In many cases your perceptions of things are based on what you expect something to be like because of past experiences and conscious thoughts. This is considered **top down processing**.

### Inattentional Blindness
- From the immense amount of visual stimuli that is constantly being processed by our eyes, we select just a few to draw meaning from.
- Failing to see visible objects when our attention is directed elsewhere is called inattentional blindness.

## Gestalt Psychology
- Was founded by German psychologists in the early 1900s.
- Premise: our minds like to see "whole" images vs. bits and pieces; the mind likes to find patterns, see relationships, categorize objects, sort and sift things.
- "The whole is greater than the sum of its parts."
- Max Wertheimer, Kurt Koffka, and Wolfgang Kohler were notable gestalt theorists.

### Law of Good Continuation
We have a tendency to group and organize lines or curves that follow an established direction over those defined by sharp and abrupt changes in direction.

### Law of Pragnanz (Simplicity)
Reality is organized or reduced to the simplest form possible.

## Depth Perception

### Monocular Cues
Require the use of only one eye

- Relative Size - Two objects of similar size; larger appears closer
- Interposition/Overlap - Partially blocked object; farther away
- Relative Clarity - Hazy objects; farther away
- Texture Gradient - Objects farther away appear fuzzy/blended, closer appear finely detailed/crisp.
- Relative Height - Objects higher up in our FOV; farther away
- Relative Motion - Nearer object; appears to move faster
- Linear Perspective - Parallel lines converge in distance
- Nearby objects reflect more light into our eyes than distant objects; dimmer appears farther away.

### Binocular Cures
Require both eyes for processing depth perception.

- Convergence - A neuromuscular cue caused by the degree to which the muscles must rotate your eyes to focus an object.
- Retinal Disparity - The difference in images as seen by our two eyes.

### The Perception of Motion
- Induced motion happens because we tend to assume that the object, or figure, moves, while the background, or frame, remains stationary.
- Stroboscopic motion is created by the rapid progression of still images such as used in filmmaking.
- Phi phenomenon

## Perceptual Constancies
- When the mind overrides what your senses are telling you.
- You know that a 6'6" tall man doesn't really get smaller just because he walks away from you.
- Or, that a red apple stays red (so you think) when the lights go out.
- Or, that the sun really doesn't get smaller when it's high up in the afternoon.

### Size Constancy
An object remains the same size despite its changing image on the retina.

### Shape Constancy
The tendency to perceive familiar objects as having a fixed shape regardless of the image they cast on our retina.

### Color Constancy
The tendency to perceive the color of an object as unchanged, even in different lighting.

### Lightness/Brightness Constancy
We perceive an object as having constant lightness even while its illumination varies.

## Priming and Perceptual Sets
- Priming happens when something prepares us to respond in a particular way.
- A perceptual set is simply a mental predisposition to see one thing and not another.