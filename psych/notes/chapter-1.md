# Research Methods in Psychology

## What Is Psychological Research?
- We use various methods to increae our knowledge base about behavior and mental processes.
- We can overcome misconceptions, hindsight bias, overconfidence, and incorrect beliefs.
- It is based on Empiricism.
    - Psychology is a **social science** (not a "hard science" or a "social study").

## Types of Reasearch
- Naturalistic Observation
- Case Study
- Survey
- Longitudinal
- Cross Sectional
- Correlational
- Ex Post Facto
- **Experimental**

## Naturalistic Observation
Watching organisms in their natural habitat without interfering

### Advantages:
- Can inspire future research
- Helps identify phenomena
- Occurs in the natural setting

### Disadvantages:
- Participants may behave differently when watched
- Not generalizable
- Doesn't suggest causation

## Case Study
In depth study of one person or small group

- Ex: Study of an individual with hyperthemesia (a condition characterized by an incredible memory for information)
- Ex: Genie the Wild Child

### Advantages:
- Can suggest futre hypotheses
- Can help develop theories
- Can help test treatments

### Disadvantages:
- Results or experiences can be atypical
- Can lead to false conclusions
- Doesn't suggest causation

## Survey
Distributed questions to many individuals where they report behaviors or attitudes.

- Ex: Surveying the habits and health problems of cigarette smokers.
- Ex: Self-Regulation in writing

### Advantages:
- Easy data collection
- Inexpensive
- Can find information on normal observable behavior

### Disadvantages:
- Wording Effect
- Social Desireability
- False Consensus Effect
    - Sample vs Population

## Longitudinal Studies **(Not in Myers)**
One group of people is repeated tested over time

- Ex: The relationship between an individual's name and their professional success later in life.

### Advantages:
- Can see changes over time
- Best way to track human development

### Disadvantages:
- Expensive and takes a long time
- Participant "attrition"

## Cross-Sectional Studies **(Not in Myers)**
Many different groups of people are studied at the same time to see how behavior/thoughts change with age

- Ex: Internet comfort in 10, 20, 30, 40, and 50 year olds.

### Advantages:
- Can give data on an entire population
- Quicker than longitudinal

### Disadvantages:
- Groups may differ in other ways leading to false conclusions.

## Correlational Studies
Examines the relationship between two or more variables

- Ex: Relationship between absences and grades
- Ex: Relationship between gender and violent behavior

### Advantages:
- Can help evaluate theories
- Can test predictions

### Disadvantages:
- **Does not confirm casual relationships!** (Illusory Correlations)

## Ex Post Facto Studies
Using **old** data to find correlation

- Ex: Relationship between church attendance and happy marriages 1998.

### Advantages:
- Can be inexpensive and do not require data collection

### Disadvantages:
- **Does not confirm causal relationships**
- Data may no longer be relevant

# The Experiment

## Experiment
One or more variables are manipulated by the researcher so that possible effects on a behavior or mental process can be recorded

### Advantages:
- Can suggest causation
- Measurable and repeatable (if conducted correctly)
- Isolates the effect of a variable

### Disadvantages
- Confounding and extraneous variables can lead to false conclusions
- Placebo effect, bias, and poor experimental methodology

## Experimental Research
- Laboratory Experiment
    - Conducted in a lab
    - Highly conrolled environment
    - Preferred by researchers
- Field Experiments
    - In natural world
    - More realistic

**Only experimental methods can determine cause and effect as they relate to human behavior or mental processes.**

## Hypothesis
- A statement about the relationhip between two or more variables.
- Must be testable and refutable.
- **Null Hypothesis** (H<sub>0</sub>): Opposite of hypothesis
    - Sometimes, instead of proving a hypothesis, scientists try to disprove a null hypothesis.

### Hypothesis Example:
- H<sub>1</sub>: Gender has an effect on perceived intelligence
- H<sub>0</sub>: Gender does not have an effect on perceived intelligence

## Variables
- **Independent Variable (IV)**: Factor that is manipulated (what you are doing to the subject)
- **Dependent Variable (DV)**: Factor that is being measured
- **Operational Definition**: Clearly define all procedures and variables so experiment can be replicated
- **Extraneous Variables**: Variables other than your IV that affect the outcome of the experiment. (There will always be some of these, but we try to limit them whenever possible. They are okay if they affect all groups equally)
- **Confounding Variables**: Variables other that your IV that affects the outcome of the groups **unequally**. (They are bad and can mess up the results)

## Control Group
- Placebo or no treatment
- Serves as a basis for comparison
- Serves to eliminate alternative explanations

## Population
- **Population**: larger group of people from which a sample is drawn

### Methods of Obtaining Population:
- **Sample**: Select a smaller group that is representative of population
    - **Random**: Every member of the population has an equal chance of participating
    - **Stratified**: Sample is put together by picking people statistically equal to the population

### Why Do We Sample?
- **False Consensus Effect**: Tendency to overestimate the extend to which others share our beliefs and behaviors
- **Sampling Error**: The extent to which a sample differs from the population

## Control Measures
- **Single-Blind**: Subject is unaware of their assignment
    - **Response Bias**: Tendency for subjects to repond to behave differently
- **Double-Blind**: Subject and experimenter are unaware of subject assignment
    - **Experimenter Bias**: Bias towards results by the human experimenter (i.e. may claim to see or stretch results)
- **Random Assignment**: Subjects are randomly selected for study and are randomly assigned a group
- **Hawthorn Effect**: Participating in a study increases the attention/performance of the group

## Reliability and Validity
- A finding is **reliable** if it can be replicated.
- A study is **valid** if it measures what is supposed to measure.

# Statistics
- **Descriptive Statistics**: Organize and summarize data
    - **Frequency Distribution**: # of occurrences of variable in a sample

### Central Tendency:
- Mean (average; most common)
- Median (arrange scores numerically and select middle score)
- Mode (score that appears most frequently; easiest)
    
### Level of Variance
- Range (distance between highest and lowest score)
- Variance (how similar or diverse scores are)
- Standard Deviation (average distance of any score in distribution from mean)

## Outliers
- The **mean** is the most commonly used measure of central tendency but can be distorted by **outliers** (extreme scores)
    - **Positively skewed**: Extreme score or group of scores that are very high
        - Has more low scores
    - **Negatively skewed**: Extreme score or group of scores that are very low
        - Has more high scores

## Inferential Statistics
- Used to assess whether results of a study are reliable or the result of chance
- Takes into account the size of sample and the difference found
- Used to test validity of hypothesis

## Statistical Significance
- Results of an experiment must reach the level of statistical significance to ensure results are not by chance.
- Psychologists accept a difference between groups to be significant when the possibility that the results are due to chance is less than 5%

## International Review Board
- American Psychological Association sets ethical guidelines and research guidelines.
- IRB gives researchers permission to begin research or require them to revise their procedures or address ethical concerns before continuing.
- All academic research must be approved by the IRB.

### Ethical Guidelines For Animal Research
1. Must have clear, scientific purpose.
2. Must answere a specific, important scientific question.
3. Animal chosen must be best suited for question.
4. Must be cared for in a humane way.
5. Must acquire animals legally/purchased from accredited companies.
6. Must employ the least amount of suffering as possible.

### Ethical Guidelines For Human Research
1. Participation must be voluntary.
2. **Informed Consent**: must know they are involved in study and minimal deception should be used.
3. **Anonymity/Confidentiality**: Privacy must be protected.
4. Participants cannot be placed in significant mental or physical risk.
5 **Deriefing**: Participants should be told purpose of study and provided with contact information.