### Parts of the eye
1. **Cornea**: Transparent outer covering that protects the eye.
2. **Pupil**: Opening at the center of the eye that allows light to enter.
3. **Iris**: Colored muscle that expands and contracts to change the size of the opening (pupil) for light.
4. **Lens**: Focuses the light rays on the retina.
5. **Retina**: Contains sensory photoreceptors known as rods and cones.
6. **Optic Nerve**: transmits information from the retina to the brain.
7. **Blind Spot**: Point where the optic nerve leaves the back of the eye which contains no photoreceptors.
8. **Fovea**: Contains most of the cones found in the refina and therefore our most detailed vision nown as visual acuity.
    - A small depression in the retina of the eye where visual acuity is highest. The center of the field of vision is focused in the region, where retinal cones are particularly concentrated.

- **Rods**: B/W Light
- **Cones**: Color

### Transduction
- Order is Rods/Cones to Bipolar to Ganglion to Optic Nerve
- Sends info to thalamus - area called **lateral geniculate nucleus (LGN)**
- Then sent to cerebral cortexes
- Where the optic nerves cross is called the **optic chasm**

##### In The Brain
- Goes to the **visual cortex** located in the **occipital lobe** of the cerebral cortex
- Feature Detectors
- Parallel Processing

### Feature Detector Neurons (Cells)
- In 1979 Hubel and Wesel discovered that there are amazing neurons in your brain called "feature detector cells" that seem to only fire when very specific stimuli is present.
- For example, some neurons in the visual cortex only fire when a diagonal line, or a certain shade, or a specific texture comes into the occipital lobe - how about "face recognition" cells?
- It is as if the eye dissects the world into thousands of "bits" of pixels and the neurons fire at very specific "bits" of information.

##### Akinetopsia:
Loss of motion perception

### Parallel Processing
- The processing of several aspects of a problem siimultaneously.

### Young-Hemholtz Trichromatic Theory
- Realized that any color can be created by combining the light waves of three primary color.
- So they guessed that we have 3 different types of receptor cells in our eyes. Together they can pick any combination of our 7 million color variations.
- Most colorblind people simply lack cone receptor cells for one or more of these primary colors.

### Opponent Processes
- The phenomenon of a negative afterimage is caused by **opponent processes**. After staring at one object for a long time, the stimulated cells use up resources, and begin to get 'tired'. Once the object is removed, these cells will have to fire below baseline for a bit to recover lost resources. Meanwhile their opponent cells will fire at baseline - at a relatively higher rate. This causes a negative afterimage.

## Hearing
AKA: Audition

### Frequency
The number of complete wavelengths that pass through point at a given time. This determiness the pitch of a sound.

### Amplitude
How loud the sound is. The higher crest of the wave is, the louder the sound is. It is measured in decibels.

### The Stimulus Input: Sound Waves
- Sound waves are composed of compression and rarefaction of air molecules.
- **Acoustical Transduction**: Conversion of sound waves into neural impulses in the hair cells of the inner ear.

### Transduction In The Ear
- Sound waves hit the **eardrum** then **hammer** then **stirrup then **oval window**.
- Everything is just vibrating.
- Then the **cochlea** vibrates.
- The cochlea is lined with mucus called **basilar membrane**.
- In **basilar membrane** there are hair cells.
- When the hair cells vibrate they turn vibrations into neural impulses which are called **organ of Corti**.
- Sent then to thalamus up auditory nerve.

### Hemholtz's Place Theory
We hear different pitches beccause different sound waves trigger activity at *different places* along the cochlea's basilar membrane.

### Frequency Theory
All the hairs vibrate but at different speeds.

### Vestibular Sense
- This sense detects our *head in motion* (and thus our body) and relays that information to the cerebellum.
- Relies on three semicircular canals in our inner ear and two small sacs that are filled with fluid and small crystal-like substances (like a small pile of stones).
- When we move left/right, up/down, or backwards/forward, the fluid and the crystals bend tiny cilia that fire a neural impulse to the cerebellum.

## Hearing Loss

### Conductive Hearing Loss
Hearing loss caused by damage to the mechanical system that conducts sound waves to the cochlea, generally the eardrum of the bones of the middle ear.

### Sensorineural Hearing Loss
Hearing loss caused by damage to the cochlea's receptor cells or to the auditory nerve, also called *nerve deafness*.

## Smell and Taste

### Sensory Interaction
- The principle that one sense may influence another.

### How do we tast?
Taste (and smell) are chemical senses.

### Taste
- We have bumps on our tongue called **papillae**.
- Tast buds are located on the papillae (they are actually all over the mouth).
- Sweet, salty, sour, bitter, and umami.

### Smell
- Particle travel up the nose to the olfactory nerve endings high in the nasal cavity.
- The olfactory bulb is actually an extension of the brain - which is why when you "snort" something it goes right into the brain crossing the BBB real fast.
- Tiny particles get sucked up your nose and chemically "read" by olfactory nerves which in turn fire neural impulses to your old limbic system.
- Smell is a very primitive and powerful system; it is the only sense that links directly to the limbic system (bypassing the thalamus).
- Women tend to have a better sense of smell than men, and they tell us humans can distinguish between 10,000 different odors.
- Pheromones are chemicals that triggers a social response in members of the same species.

### Smell and Memories
The brain region for smell (in red) is closely connected with the brain regions involved with memory (limbic system). That is why strong memories are made through the sense of smell.

## Skin
- Big four skin senses are: pressure, warmth, cold, and pain.
- We don't have receptors for: wet, smooth, rough, yucky, slimy; if not, then how can we detect them.

### Receptors

Receptor | Stimulus | Percecption
:-: | :-: | ---
Merkel Receptors|Pressure|Pressure
Meissner|Light Tapping|Flutter
Ruffini|Stretching of Skin|Stretch
Pacinian|Rapid Vibration|Vibration
Free Nerve Endings|Increase/Decrease in temp|Warmth - hot - cold
Nociceptors|Intense pressure, extreme temperature, or burning chemicals that can damage the skin|Pain

## Pain
Pain tells the body that something has gone wrong. Pain originates at the point of contact. The message is then sent from the point of contact up the spinal cord to the thalamus in the brain. It is then projected to the cerebral cortex, where the person registers the location and severity of the pain.

Neurotransmitters called **prostaglandins** communicate the pain message.

Drugs like aspirin curb the production of prostaglandins and help alleviate pain.

### Influenced by
- Amount of Pain
- Personality
- Social Support
- Situation - Context
- Drugs
- Substance P
- Endorphin Release
- Culture
- Cognitive Appraisal

### Gate Control Theory
- At the level of the spinal cord there are neurological "gates" which open or close depending on the signal coming in.
- Pain fibers are small & fast and will "open" the gate sending their signal on to the brain.
- Other sensations such as cold or pressure have larger fibers and "close" the gate to pain and send their signals to the brain.
- Thus, when you put ice on a bee sting, the "cold" sensation (moving on larger nerve fibers) wins out over the "pain" sensation, or when you put pressure on an injury, you feel pressure and not the pain.
- Endorphins but may even "shut off" the nerological pain gates at the spinal cord.