# A Beautiful Mind

This film is about John Nash, who won a Nobel Prize for his contributions to game theory.
When he is visiting Princeton University, where he graduated from, he meets an old college roomate, Charles, and his niece.
He also runs into an agent for the US Department of Defense who invites him to work on cryptography for the government, then giving him an assignment to search newspapers for clues about a Soviet Plot.
He thinks he is being watched while giving a lecture and runs away, being knocked out and sent to a psychiatric ward.
He thinks that they are Soviets trying to get information from him until his wife convinces him that the agent and attackers were hallucinations.
Eventually he realizes that Charles and his niece were also hallucinations.

The problems Nash experiences stem from Paranoid Schizophrenia. This disorder causes the affected person to experience delusions and hallucinations.
Delusions are beliefs held extremely stongly despite evidence pointing otherwise. Nash's ideas that he was working for the government and being hunted by Soviets were delusions.
The hallucinations seen in the movie were visual and auditory. However, real life cases of Paranoid Schizophrenia, including Nash's, are usually only auditory.

I'm personally not a very big fan of this movie. There is something about the general feel of the movie that I don't care for.
It provides a decent look into some of the troubles John Nash went through in his life, but not entirely accurately.
As meantioned earlier, Nash and other people experiencing paranoid schizophrenia wouldn't have likely experienced the lifelike visual hallucinations depicted in the film.
I think it holds up decently as an interesting film about psychological problems that some people face, as long as you know that it isn't entirely accurate.
