# Unit 1 - A New World

# The First Americans
- Bering Streight Migration around 40,000 BCE
- Migrated with the changing climate and food supplies (Woolly Mammoth and Bison)
- Developed Agriculture around 9,000 BCE
- Relied mainly on corn, squash, and beans
- Settled into societies with unique cultures

## Population Centers
- Largest populations in Mexico (Aztecs) and Andes (Incas)
    - Learned agricultural techniques much earlier
- Absence of livestock limited farming (plowing/fertilizer)

## Common Characteristics

| **Religion** | **Land and Property** | **Gender Roles** |
| --- | --- | --- |
| Animism/rituals | Land is a common resource | Men led, but women still had important roles |
| Believed in one creator | Families given plots of land to use for a season | Women could divorce |
| | | Women could own tools/dwellings |

## Mound Builders of the Mississippi
- Built large government buildings/trading centers
- Large trading networks extending to Ohio River
- Cahokia: Society located in St. Louis; largest settles community in US until 1800
- Built enormous burial mounds

## Western Indians

### Southwest (pueblos)
- Traded with Mexico and Mississippi River Valley
- Built huge water networks (canals and dams)
    - Perfected irrigation for corn, beans, and cotton

### Pacific Coast
- Hunter/gatherers
- Fished, hunted sea mammals

### Great Plains
- Hunter/Gatherers
- Very Nomadic
- Dependent on Buffalo

### Eastern North America
- Frequently warred with one another for resources, seized captives, or revenge
- Conducted diplomacy
- No central authority until 15th century
- **Great League of Peace:** five Iroquois nations (Mohawk, Oneida, Cayuga, Seneca, and Onondaga) unite

### Aztecs
- Located in modern day Mexico
- Extremely advanced
    - roads, aqueducts, pyramids, advanced astronomy
- Tenochtitlan Population around 250,000
- *super* violent

### Incas
- Located in modern day Peru
- Also incredibly advanced
    - Roads, bridges, high-altitude agriculture
- More chill than the Aztecs, but were still warriors

# The Europeans Arrive

## What's Happening in Europe? (the positive)

| **The Enlightenment** | **Scientific Revolution** | **Renaissance Wealth** |
| --- | --- | --- |
| Philosophers question government and religious institutions | Helps create the enlightenment | Nations had more money to spend
| Promote ideas about "natural rights" and freedom | Better Technology (especially for navigation) | Nations also wanted to dominate trade
| | | Italians get really good at sailing |

## What's Happening in Europe? (the negative)

| **Religious Persecution** | **Social Immobility** |
| --- | --- |
| Catholic Church reasserts its control | Wealth controlled by traditional, long-standing aristocracy |
| Non-Christians being killed, persecuted (e.g. the Moors) | Very difficult to rise through social classes |
| Witch Trials | |

## Columbus
- Chartered ships from Ferdinand and Isabella of Spain (same year as Reconquista)
- Strong religious and financial motivations
- Oct 1492: Lands in Hispaniola (Haiti, DR, and Cuba)
- Returns in 1493 with 14 ships, and 1,000 men.

## Effects of Columbus' Voyage

### Exploration Explodes
- "a hero such as the ancients made gods of"
- Gutenberg's printing press made rapid spread of information possible
- Expeditions from Portugal, Spain, England, and France shortly after

### Columbian Exchange
- Transatlantic flow of agricultural goods between the "Old" and "New" Worlds
- Also brought a TON of death via smallpox to Old World populatinos (app. 80 million)

## Spanish Domination
- Dominated colonizaion in the New World
- Mantra = God, Glory, and Gold
- Conquistadors: Notorious Spanish explorers the used ruthless tactics to conquer lands

| **Hernan Cortes** | **Francisco Pizzaro** |
| --- | --- |
| First to make contact with major civilization (Aztecs) | Conquered Incas |
| Conquered the extremely advanced society with superior weapons, tactics, and **disease** | Took Incan king hostage; killed him even after receiving ransom (classic) |
| | Again, mostly aided by smallpox in wiping out empires |

### Government
- Kept central power in the hands of the crown
    - Reflected absolutism
- Spanish run by lawyers/bureaucrats (viceroys); Did not allow creoles to hold office
- Church oversaw Indian relations
- Civil law code

### Economy
- Encompassed richest and most populous regions
- Based on gold and silver; transported to Spain and Asia via galleys
- Used Indian slave labor for mine work
- Gold supplied haciendas: large-scale farms
    - Still farming new world crops

### Indian Relations
- Enslaved Indians... so not great, but alo not the worst
- *Encomienda System*: Spanish feudal system based on race
- Sought to convert natives to Catholicism (Virgin Guadalupe)
- Wanted Indians to assimilate
- Pueblo Revolt

## Causes for more permanent settlements

### Political Stability
- Spain unified under Ferdinand and Isabella
- England calms down under Elizabeth I
- Netherlands always been pretty chill, but now they have a lot more money

### Capitalism
- **Joint-stock Companies**
    - Increased initial funding for colonial voyages
    - Lessened risk
    - Increased profits for everyone
- Capitalism over Feudalism
    - make money instead of given land

### Population
- **Columbian Exchange**
    - more food for Europeans
    - more Europeans
- **Enclosure Movement**
    - mad small farming impossible

### Mercantilism
- Mother country controls trade/commerce
- Purpose of the colonies is to supply mother country with raw goods

## Europeans and Natives

### Results of Spanish Treatment
- Extended contact with Natives made led to debate among Europeans of how they should be treated
- De Las Casas vs Sepulveda
- **The Black Legend**
    - The image of Spain as a uniquely brutal and exploitive colonizer
    - Provided justification for other European powers to challenge Spain's predominance in the New World
- All Europeans felt they were liberating the native peoples

### Europeans "Save" The Natives

| **Religion** | **Property** | **Culture** |
|---|---|---|
|"Where the spirit of the Lord is, there is Freedom"|No concept of private property: fundamental concept to European liberty (esp. British)|General savages|
|European Freedom = life without sin|Europeans justified taking land in Americas, because Natives were not using it|Native women seen as "mistreated"
|Natives were not Christian; therefore worshiped the Devil||Men only fish and hunt (leisure activities)|
|||Tribes follow Chieftain, would never challenge a despot|

### Natives fight back
- Europeans enslave natives, force European culture/religion, and/or encroach too far
- Natives usually not unified enough to fight back
- Pueblo Revolt
    - Successful revolt in New Mexico
    - Drove 2,000 settlers away

## Europeans and Africans

### Slavery in Africa
- Slavery everywhere in the world
- Traditionally, African slaves were criminals, debtors, or POWs
- Slaves had rights, could gain freedom
- Did not define the economy

### Portuguese
- Began importing African slaves to Azores/Canary Islands to work sugar plantations
- Over 100,000 African slaves imported to Portugal and Spain between 1450-1500

