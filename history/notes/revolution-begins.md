## Revolution Begins

### Continental Army
- George Washington appointed (by Adams) to lead Continental Army
    - Very weak compared to British
    - Outnumbered, and out-resourced
- Washington's plan for winning the war
    1. Never face the British head-on
    2. Make the war too expensive (lasting too long) for the British to fight
    3. Spies
    4. Be a wizard

### First Years of the War
- Contintental Army suffers huge losses
- Many soldiers killed, or desert
- Patriots lose NYC early in the war
- Benedict Arnold defects to British

### Washington gets a W
- Washington's Crosses the Delaware
    - Washington crosses icy Delaware River from PA to NJ
    - Sneak-attacks Hessian mercenaries on December 25<sup>th</sup>, 1776 (Battle of Trenton)
    - Huge victory for the Patriots

### Patriots Lose the Capital
- British capture Philadelphia in 1777
    - Washington isn't able to fortify city in time
- Continental congress forced to leave the city

### New Allies
- Other half of British army marching down to Philly from Albany, NY
    - Plan to cut off New England from other colonies
- Patriot General Horatio Gates destroys British Army at Saratoga
- Meanwhile: Ben Franklin, Thomas Jefferson, and John Adams in France, convincing them to join the effort
    - French obsessed with Ben Franklin
    - Saratoga helps France justify joining the war on the side of the Americans

### Valley Forge
- Washington fortifies Army in Valley Forge, PA
- Winter of 1777 extremely brutal
    - 2,000 out of the 12,000 troops die from disease, exposure, or malnutrition
- Washington meets two important military leaders
    - Baron von Steuben: help retrain troops, new sanitation strategies; turn Patriots into a real army
    - Marquis de Lafayette: French officer who eventually convinces France to send more troops

### British Change Strategies
- British move to southern strategy; focus on Virginia
    - Believe there are more loyalists there
        - (They were wrong)
- Control most of the South from 1778-1780
- At this point, France enters the war; changes everything

### Battle of Yorktown
- War already becoming unpopular in England
    - And Expensive
- British General Cornwallis targets Yorktown, VA
- French and American troops surround the British (sea and land)
- British trapped; surrender after 3 weeks (October 17, 1781)

### Treaty of Paris
- 1783 Treaty of Paris officiallyy ends the war
    - 2 years after the Battle of Yorktown
- British recognize American Independence, and establish New Borders