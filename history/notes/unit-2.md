# Unit 2 - England Colonizes

## Problems at home

### General Misery
- Overpopulation
- Enclosure Movement
- Bad economy
- Vagrancy laws
- Religious uncertainty
    - Battle between Protestant and Catholic
- Power struggle between Monarchy and Parliament

### No Land
- Land was fundamental to freedom
- Economic freedom (more money, less problems)
    - Be your own boss
    - Voting rights
    - Pass on to Children
- Land = Independence
- 16<sup>th</sup> C: 2/3 arrive to Chesapeake as Indentured servants

### English Freedom
- Conocept evolving since Magna Carta (1215)
- *"All Englishmen were governed by a king, but 'he rules over free men' according to the law" unlike the autocratic monarchs of Europe*
- Enlightenment further questions the role of Government
    - Hobbes and Locke

### New World Prospects
- England had half the population of Spain, but twice the people willing to go to the new world.
- Spain and France already had the best land
- Leftover land looked much like England
- Support from Crown
    - Cavallers

## First Colonies: Jamestown

### Settlement
- 1607: Charter by the Virginia Company (joint-stock)
- Mix of wealthy and working-class men in search of economic opportunities
- Huge disaster
    - Tons of diseases
    - Half the population wouldn't work
    - Starving times
    - Only 20% survive

### Settlers Adapt
- John Smith initiates military rule
- Give up search for gold; turn to Tobacco production (John Rolfe)
- Adopt agricultural economy
- **Headright system**
- 1619 First slaves arrive

### Tobaco Boom
- Crown could collect taxes
- Colonists seek more land/permanent settlements
    - Harms relationships with Natives
    - Rebellion 1622
- Economy becomes heavily reliant on cheap (and free) labor

### New Government
- Virginia Company could no longer oversee Jamestown
- 1624: Becomes Royal Colony
    - Governor chosen by Crown
- England in semi-chaos, didn't care as much about colonies (begin **Era of Benign Neglect**)
- 1619: Establish **House of Burgesses**
    - Sets precedent for British colonial governments

### Results
- Economy First
- Agricultural/Land dpendence
    - Dependence on Slave Labor
- Huge divide between rich and poor
    - New Elite class
- Representative government run by the plantation elites

## First Colonies: Plymouth/Massachusetts Bay

### Puritans
- "Real" Christians
- Thought Church of England was too Catholic
- Sought to create a "City upon the Hill" to help influence Church back home
- Hard Workers
    - Idleness and immoral behavior were sure signs of damnation

### Settlement
- 1620: Mayflower expedition funded by English investors
    - "Holy Experiment" - City upon the hill
- 1629: MA Bay Colony (also Puritans)
- Brought fro families: equal men and women
- Settled in a cleared old Native American settlement in Plymouth, MA

### Setlers Adapt
- Land not as diseased, but not great for farming
    - Squanto aids in survival
    - 1621 Thanksgiving
- Land shared equality
    - Most families given land, som farming
- Diverse group of settlers; diversified economy
- Developed trading and Merchant class
- **Schools**

### Government
- **Mayflower Compact**
- Representative Government in each town
- Run by freemen
- Protected public good over economy

### Results
- Diverse Economy
    - Hard work rewarded
- Common good above economy
    - Wealth more evenly distributed
    - Minimal need for free labor
- More direct representation
- Super educated

## Middle Colonies

### The Dutch
- Settled in most of middle colonies (New York, Pennsylvania, Delaware)
- 1664: New York seized by English after Anglo-Dutch war
- English continued to allow religious toleration; attracted tons of great settlers
    - But less freedom for blacks
- Some "rights of Englishmen" initially denied
- Government: royal governor; controlled mostly by landed elite