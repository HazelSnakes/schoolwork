# Unit 3 - Colonists Get Woke

### Great Awakening
- Creates a sense of unity between the colonists for the first time
- Encourages a challenge of authority
- Intellectual exchange of (written) ideas
- Catalyst to creating major universities

### Education
- Educated professionals: ministers, physicians, and lawyers
- American colonists most literate individuals in the world
    - 3/4 free adult males, over 1/3 of women, could read and write
- First library opened by Ben Franklin in 1731
- Newspaper publications 1725: 5
- Newspaper publications 1776: 40+

### Rights of Englishmen
- English Revolution solidifies Parliamentary sovereignty
- English proud of their unique liberty
    - Elected officials consenting to the will of the people
    - Restraints on arbitrary exercise of political authority
    - Trial by jury in the common law
- Checks on powers of government
- Montesquieu: Britain "the one nation in the world whose constitution has polical liberty for its purpose"
    - No man, even the king, is above the law

### Republicanism and Liberalism

**Republicanism** | **Liberalism**
--- | ---
Public and Social Liberty | Private and individual liberty
Promoted Representative Democracy | Belief the government is necessary to protect individual liberties; but that government can also infringe on liberties
Promoted land-owning "virtuous"<sup>[[1]](#virtuous)</sup> citizens to be active in politics | Individuals must protect privacy/private matters from the government
The citizens are the sovereign | The citizens are the sovereign

### John Locke
- Two Treatises of Government (1680)
- Government is a "social contract" between the people and state
    - State is supreme, but it is bound to follow "natural laws"
    - Necessary for citizens to surrender some rights, in order to protect liberties
    - Never surrender natural rights
        - Life, Liberty, and Property
- Sovereignty ultimately rests in the people rather than the state
- Citizens have a right and an obligation to revolt against whatever government failed to protect their rights

### Politics in Culture
- Colonists *loved* reading and debating Enlightenment philosopher's ideas
- Everyday colonists educated in political philosophies
- Intellectual clubs in Boston, New York, and Philadelphia
- Meeting places/taverns become centers for political debate
    - "The poorest laborer thinks himself entitled to deliver his sentiments in matters of religion or politics with as much freedom as the gentleman or scholar"
- Became the culture to protest "tyranny"

### Salutary Neglect (Benign Neglect)
- Britain allowed colonies to largely govern themselves
    - Most assemblies controlled by wealthy landowners/merchants
- Still had royal governors in most colonies, but controlled by assemblies
    - Royal Governor salaries paid for by colonists
    - Colonial assemblies set restrictions on salaries, unless the governor "played ball" with them
- As wealth grew in the colonies, colonial assembly power grew
- Began demanding more liberties (like the House of Commons)

### The Press
- Everyday citizens did not have freedom of the press/speech
- After 1695, government could no longer censor newspapers, books, and pamphlets before they were printed
    - Authors and publishers could still be prosecuted for "seditious libel"
        - Criminal act of defaming a government official
    - Colonists fought back; believed freedom of the press was crusial to liberty
        - Few newspapers attacked colonial governments
    - Zenger Trial

## Aftermath

### 1763 Pontiac's Rebellion
- Indians wanted to stop British fromm moving on their land
- Attacked thousands of settlers
- English help fight back; win

### Betrayal
- Establish Proclamation Line of 1763: Prohibited colonists from moving west of the Appalachian Mountains
- Adds further insult by taxing colonists for the war debt
- British troops permanently stations in America
- Beginning of the end of **Salutary Neglect**

### Home Invasions
- British especially interested in stopping smuggling
    - (illegally bring items into an area without paying taxes on it)
- **Writs of Assistance**: allowed British tax collectors to board ships and enter businesses of traders
- Some traders worked from home **∴** Tax collectors could search private homes.

## Sugar Tax
- 1764 Sugar Act: Very high tax on molasses from non-British lands
    - Forced Colonists to buy molasses and sugar from British colonies
    - Tried in hostile British courts (without juries)
- Enforced Navigation Acts: Required colonists to only ship to British ports

## Stamp Act Crises
- 1765 Stamp Act: Taxed most legal documents, newspapers, pamphlets, almanacs, and othe documents printed on special paper that had a stamp on it
- Meant to help fund military
- Almost every colonist affected
- First time British directly tax colonists
- Met with extreme opposition/protest (on both sides of the Atlantic)

### Colonial Responses
- Colonists super angry
- **Sons of Liberty**: radical group formed by Samuel Adams, John Hancock and other radicals in response to the Stamp Act
    - "Liberty, Property, and no stamps"
    - Harassed British government in Boston
    - Hung effigy of tax collectors

<a name="virtuous">1</a>: Virtue = Moral; Willing to sacrifice self-interest in pursuit of public good