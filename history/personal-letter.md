My name is Edwin Snider, though people generally call me Quin. I live with my mom and little brother, as well as my mom's boyfriend, Clay.
Also living with us on and off are my younger step-brother and clay's two children, Hank and May.
One memorable moment from my history is when I first began learning how to program using python.
It's where my interest in technology and computers really began to take shape.

While I will admit that the main reason I chose to take this class is because of teacher recommendation, I do have other reasons.
I find history to be an interesting subject in general, but I many history classes have been somewhat uninteresting in the past.
I'm taking this because I hope to get a more in depth look at history rather than surface level.
However, I am somewhat worried about taking 3 different AP classes this year because my mental health is kind of at an all time low as of late.
I do still plan to do my best at getting all my work done properly and on time.

I am fond of the quote, "Intelligence is the ability to avoid doing work, yet getting the work done." by Linus Torvalds.
It is saying that it is good to be able to find out the best way to do do less work to get something done, but still end up with just as good a result.
A personal example of this is the small system I have set up for writing documents for school.
I write them in simple markdown files with minimal formatting, then run a script I wrote to output them to pretty pdf files with LaTeX.
