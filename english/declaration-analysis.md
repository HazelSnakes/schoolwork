# "Declaration of Independence" Analysis

1. List five words that you felt displayed connotative diction (i.e. words that Jefferson uses to either incite emotion or lesson the blow of oppression). Beside each word, describe why you chose this word. In this discussion, consider what other word(s) could have been used.
    - **Insurrections** - 
    - **Merciless** - The word "merciless" used along with "savage" gives a beastial connotation to those they are describing, creating both slight fear, and a feeling of being "above them".
    - **Free** - Using the word "free" as opposed to "independent" helps to imply that they are not free, imprisoned under the king's rule.
    - **Justice** - Helps to villainize the king further, by saying that justice must be brought to him. 
    - **Invasions** - This word helps give the feeling that Britain is unwelcome and imposing on them against their will.