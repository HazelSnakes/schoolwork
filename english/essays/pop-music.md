---
firstname: Hazel
lastname: Snider
professor: Mrs. Jennifer Jones
class: AP English Language And Composition 11
bibliography: bibs/pop-music.bib
nocite: |
  @stauber_small_2018
title: Pop Music
---

To many, it seems like the music being made today has been getting less unique, more boring as time goes on.
Brian Wilson of The Beach Boys says that "Pop music has been exhausted ... we've lost the ability to be blown away by music" (Wilson).
This is not completely true; there are plenty of newer artists and pieces of music, even in the pop genre, that continue to create stunning music.
However, there are a few reasons that this music isn't found as often and doesn't sell as well as the samey music found everywhere.
Music with less variety is known to sell better, so companies with the ability to release and market music in mass quantities choose not to take the risk of making music with more variety.
Another part of the problem has to do with the way we find and listen to music. Services such as Spotify and YouTube use algorithms to present you with music similar to what you already listen to.

Music is not getting worse overall. There have been many great artists and songs in recent years you just need to know where to look.
Take for instance, the amazing work of Jack Stauber. He manages to combine pop-like lyrics and style with the use of more bizarre instruments more musical variety within the songs themselves.
His song "Small World" goes through four distinct tones and melodies, something that doesn't happen very often in the pop music most people know about.
There are also many other artists that create wonderful music in modern times. The problem isn't that good music isn't being made, it's that people aren't able to easily find quality music.

The first thing contributing to the problem of it being hard to find good music are the companies producing music.
The companies with the money and power to create and market lots of music to mass audiences create music with little instrumental or stylistic variety.
This is most likely because music with less instrumental variety has been shown to sell better [@percino_instrumentational_2014, fig. 5].
These large companies would rather create more of what they know will sell than experiment to find better and more unique music.
Because they have so much money available, they are able to drown out smaller artists by sheer volume of music produced.

Another major factor that makes it hard to find interesting and unique music is that the services we use to listen to music make it hard.
Streaming services such as Spotify, iTunes, and YouTube use algorithms to recommend new music to their listeners. However, there is some fault in the way these algorithms serve recommendations.
They work by looking at what the user already listens to and likes, then finding more music that is similar. This seems like a solid idea in principle, and it most likely does increase revenue,
however, this means that unless people go out of their way to find more variety in music, they are going to be shown more of the same.

In conclusion, music has not gotten worse overall in recent years. The music known to the majority of people has gotten worse.
There are many great artists to be found if you know where to look such as: C418, Jack Stauber, Chris Christodoulou, and many others.
If you are bored of the music of today that seems to be all the same, there is great music out there being made all the time.
Try to seek out new and interesting pieces of music. You simply have to take the time and know where to look.

# References
