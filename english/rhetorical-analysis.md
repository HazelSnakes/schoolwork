# Rhetorical Analysis Techniques & Strategies

- Imagery
- Example (personal/real life, historical\[allusion])
- Connotative Diction
- Syntax
- Figurative Language (metaphors, similes, personification, hyperbole)
- Repetition (anaphora)
- Appeals (logical, emotional, character)
- Juxtaposition/antithesis/paradox
    - Antithesis: "An unjust law is no **law** at all."
    - Paradox: "I am nothing; I see all"

1. What is the message?
2. How does a writer construct his/her words to deliver the message?