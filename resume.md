# Elements of a Great Resume

## Contact Information
### Must Have
- Name
- Adress
- Phone Number
- Email
    - **Professional**
    - Typically your first and last name

## Objective
Most professionals agree that this never helps and often hurts your resume.
- Typically found on a resume template, simply delete it.

## Education
### General rule of thumb
- If **college** is on your resume, delete high school
- If you are not going to college, leave on your HS information until you are at least 5 years out of HS, or have built up a strong job experience.

### What To Include
- Name of school and location, year of graduation, degree received, GPA (only if above 3.5)

## Skills and Abilities
- Keywords are important here
- Use this to highlight your top qualities and software you're familiar with
- Easiest section to tailor to the job posting

## Job Experience
- Aim for the last 10 years of work experience
- If no job experience, create a section listing volunteer work
- Reverse Chronological Order (most recent job first)

### Must Include
- Company
- Job Title
- Location (City.ST)
- Dates
- Description

## References
- Never include references on your resume, these belong on a seperate page.

### Don't Include
- Hobbies
- Pictures
- Emojis
- Crazy Color
- Hard to read or tacky fonts
- Spelling mistakes
    - Use spell check
- Mismatched or odd formatting