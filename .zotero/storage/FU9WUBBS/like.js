(function($) {
    'use strict';

    $(document).ready(function() {

        edgtfLikes();

    });

    function edgtfLikes() {

        $(document).on('click','.edgtf-like', function() {

            var likeLink = $(this),
                id = likeLink.attr('id'),
                type;

            if ( likeLink.hasClass('liked') ) {
                return false;
            }

            if(typeof likeLink.data('type') !== 'undefined') {
                type = likeLink.data('type');
            }

            var dataToPass = {
                action: 'magazinevibe_edge_like',
                likes_id: id,
                type: type
            };

            var like = $.post(edgtfLike.ajaxurl, dataToPass, function( data ) {

                likeLink.html(data).addClass('liked').attr('title','You already like this!');

                likeLink.children('span').css('opacity',1);
            });

            return false;
        });
    }

})(jQuery);