
logo

    Home
        Forums
    News
    Music
        NRW Radio
        Releases
        Retrowave Articles
        Featured
        Album Reviews
        NRW Records
        WeRuleNation
    Video
        Videodrome
        Retro Movie Reviews
        Sctron Entertainment
    Art
        Photography
        Retrowave Artwork
        Comic Reviews
    Gaming
        Game Reviews
        Gaming Videos
    Store
        Akade Wear
        NRW Records Store
    Contests & Competitions
    About Us
        Our Team
        Contact
    Submissions
    Sign In/ Register

logo

    Home
        Forums
    News
    Music
        NRW Radio
        Releases
        Retrowave Articles
        Featured
        Album Reviews
        NRW Records
        WeRuleNation
    Video
        Videodrome
        Retro Movie Reviews
        Sctron Entertainment
    Art
        Photography
        Retrowave Artwork
        Comic Reviews
    Gaming
        Game Reviews
        Gaming Videos
    Store
        Akade Wear
        NRW Records Store
    Contests & Competitions
    About Us
        Our Team
        Contact
    Submissions
    Sign In/ Register

MENU
mobile-logo

    Home
        Forum
        NRW Guestbook
    News
    Music
        NRW Radio
        Releases
        Retrowave Articles
        Featured Album
        Album Reviews
        NRW Records
        WeRuleNation
    Video
        Retro Movie Reviews
        Videodrome
    Art
        Photography
        Retrowave Artwork
        Comic Reviews
    Gaming
        Game Reviews
        Gaming Videos
    Contests & Competitions
    NRW Store
        Akade Wear
        NRW Records Store
    Submissions
    About Us
    Sign In/ Register

Home  /  Home  /  An Interview With Jack Stauber


Home Interview Music News NRW Exclusives Retrowave Articles
An Interview With Jack Stauber

Jack Stauber is certainly not our usual synthwave sound, but when we came across his masterpiece "Pop Food," we knew immediately we should feauture his art! Born April 6, 1996, Jack Stauber is an American Avant-Pop musician and video artist whose work includes elements of
20th July 2018
12:22 pm
Andrew Zistler
0
6
23615 Views

Jack Stauber is certainly not our usual synthwave sound, but when we came across his masterpiece “Pop Food,” we knew immediately we should feauture his art! Born April 6, 1996, Jack Stauber is an American Avant-Pop musician and video artist whose work includes elements of animation, live performance, and the use of obsolete media.

His work includes themes of absurdity, bizarre humor, and nostalgia. Stauber has independently released 4 albums: Finite Form (2013), Viator (2015), Pop Food (2017), and HiLo (2018) . He remains unsigned. Stauber is currently based in Pittsburgh, PA.

Your vocal style is truly one of a kind – how did you develop it?

I sing in the shower. I try to do effects with my voice a lot, sort of like Donovan does in “Hurdy Gurdy Man.” He goes “hu hu hu hu hu hu hu hurdy gu hu hu hu hu hu hu.” He sounds like he’s singing on a bumpy car ride, but he’s not in a car at all.

Many of your lyrics border on nonsense, but somehow at the same time they can also feel very meaningful – how do you go about writing them?

I get playful with the words but they’re always chosen very carefully. They all make perfect sense. I wouldn’t sing something if it didn’t.

How much Jack Stauber music is the result of playing a traditional instrument – and how much of it is sampling as seen in “Can Music?”

I definitely have a good mix of textbook sound-makers and off-the-book inserts. I’ve crinkled an empty water bottle to create a crash cymbal before. I collect tonal objects that I find to pepper into songs here and there. I wouldn’t say I use these as song foundations usually, though, and if I do, the song’s gonna be an odd one.

Speaking of instruments – various frames in your videos betray a handsome synth collection – what are some of your favorite synths you use to produce your music?

I like small casiotones. MT-240 is my main squeeze right now. The sounds are warm and stupid like a cheddar pretzel.

It  also seems like you might use a lot of “found” digital devices as ad-hoc instruments – do you have any particularly odd devices you’ve grown fond of producing with? If so what are they?

There’s this cork slide whistle I bought at the mall for a dollar. I love that slide whistle. It’s in the latest song I extended on my patreon page, “Cheeseburger Family.” Walk around with a slide whistle and your life turns into a cartoon.

The music you create feels like only half of your style – when did you start creating short videos and other media? Do you prefer creating audio or visual?

I’ve been working with both forever, I’ve always loved animation and video. They have different kinds of rewarding elements. I get equally sucked into making them both as well.

In the past year you’ve released more than a video per week, on average – how do you find the time to produce so much content? Or – much more importantly than “getting into the minutes” – how do you find the drive?

Gee I don’t know. I love making these things so much. I drew this baby the other day for a cartoon and laughed at the way one of his faces looked for a while. I love that I can do that right now, and I will do everything in my abilities to make sure I can keep giving these projects the attention I need to complete them. Art is a big coping method for me, too. She’s my outlet. If I feel down, I make something. If I feel happy, I make something.

Do you do all your own voices for your shorts? All your own animation? What is your process for creating all those little interesting characters?

I do for the most part, yes. I’ll record the dialogue with a slight idea of what they may look like. Then I run the audio back with my eyes closed. I think it’s just putting a name to a face from there.

I draw my animation bits and pieces in MS Paint with my mouse. I have folders on my computer filled with hundreds of individual saved MS Paint frames. I sequence/arrange those in Adobe Premiere, and then I take the video and run it through a VHS tape.

 

It seems like your creation switch is permanently flicked on. Do you have any advice for other artists struggling with burnout? …How much unreleased Jack Stauber media do you think you’re sitting on right now?

I am sitting on plenty of unreleased media. The things that see the light of day see it for a reason. I make songs every day. I’ve considered dumping the reserves in a wild fat release some day to free up my shelves.

If someone isn’t feeling particularly inspired, I’d recommend preparing a good setup for impending ideas. There’s a chair in my bedroom that I’ve surrounded with a little casio, my computer, pencils, paper, and a microphone. Within reach is a drawer filled with various noise-makers I’ve collected, and above that is a drawer of old AV equipment. I’ve thrifted a few tape recorders that sit next to different keyboards in case I feel the urge to make a demo. Being ready when you don’t need to be is better than not being ready when you finally have a cool idea.

Past that, taking time out to read about topics of interest and learning new tricks from tutorials online are great ways to rev the engines, as well. Visit the library and read anything.

Much of your early work from Finite Form and Joose has a very raw indie / alternative feel – and with Viator we hear the song lengths shorten and your music becoming more experimental. What was the catalyst for this change of form?

I made Viator while dealing with a lot of loss. Making it helped so much. Every night that summer I made a new song. While making it, I had no initial plans of releasing it. Some of the foundation guitar tracks in Viator were completely improvised. Actually, some were sung over improvisationally and written entirely in one shot. I’d listen back to them and realize dusty stuff that had been sitting on my brain for a while.

Your latest releases, “Pop Food” and “HiLo” are similarly experimental, but feel much more accessible. What made you want to take inherently weird sound and infuse it with pop?

In 2016 I began playing live with a keyboard and a looper. I was recording and producing quite a bit, too. A lot of the stuff from this time is unreleased. I was writing with a lot of bitterness towards pop music. I have this one song from that year where the lyrics are mostly the word “girl” being repeated the whole time over some pop chords. I’d made a bunch of these really bastardized electronic tracks that year with odd instrumentals and ridiculous pop lyrics. One of these songs was called “Assley Tearsdale.” It got a rather rough response, so I tried changing the funky instrumental with a super slicked up pop one and added a repeating synth riff. I still play this version today at live shows. Performing and expressing the feelings behind those songs gave me tons of energy. I learned a lot from this time period from a technical standpoint, and most importantly, it got me thinking about pop as a language. The rest of the year I focused on making stronger instrumentals which I also piled up and have sitting somewhere. I got my first electric guitar at the beginning of 2017, and plugging that into an interface entirely changed the way I produced music. The timing was perfect and Pop Food just flew out.

You’ve been creating since high school – what musical or visual artists influenced you in those early years? What artists or art influences you now?

I was always watching The Wall. I still love that movie. Pink Floyd. I think about Bruce Bickford’s stuff a lot, too. Loved the Mighty Boosh. My parents raised me around a lot of great music. Mom fed me campy 80s alt and Dad gave me the good ol’ Dead and jazz-funk.

In high school I listened to a ton of art rock and experimental pop. Digital Shades Vol. 1 and The Residents really blew my mind. They still do.
I got into a bunch of freak prog and goofus rock, and now I’m here.

My favorite cartoon was and still is Rugrats. Take some time to watch a minute of the pilot of Rugrats if you haven’t seen that before. The art style is beyond incredible. Mark Mothersbaugh is doing, (and NAILING,) the score, (I love Mark’s muzik work and Devo, as well,) and the whole thing put together is brilliant. I have that pilot as a bonus feature on a VHS I thrifted a while back. A prized possession.

I love older cartoons, too. When I was really little I watched this tape of Fleischer cartoons and I was hooked for life. Merrie Melodies and the Pink Panther make my heart beat steady.

What can we expect from Jack Stauber in the future?

Micropop, the wailing of synths and stabbing 707 snare hits! Tragic echoes! Sentient music running around with hands and legs! Expect the careful thoughts of old dead people packaged in a CRT, zooming out of your favorite gizmo and pinching the nipples of your heart! Expect a warm confusion, and a hand to hold on the way down!

POST TAGS:
Finite Form freak folk hi lo hilo indie Interview Jack Jack Stauber pop food Stauber synthpop Viator
Andrew Zistler

andrew.zistler@newretrowave.com
Review overview
RELATED ARTICLES
ollie wride cover art
FM-84’s Ollie Wride’s Debut Single “Overcome” Doubles Down on Upbeat Pop
Aug 24, 2018
0
0
Human Music 2 Festival Recap
Jun 22, 2018
0
3
Mystvries Releases Debut Single – Never Know
Jun 15, 2018
0
0
Loading...
POST A COMMENT Cancel Reply

You must be logged in to post a comment.

This site uses Akismet to reduce spam. Learn how your comment data is processed .
Search for:
NRW Login
Log In
Username:
Password:
Remember Me
Log In
Register Lost Password

    NRW Brand and Mission Statement
    FAQ
    NRW Disclaimer
    Legal Claims
    Terms of Use
    DMCA Takedown Notice
    Advertising
    Licensing Music

Twitter
Tumblr
Instagram
Facebook
Soundcloud
Youtube
©2018 NewRetroWave, LLC. All Rights Reserved
