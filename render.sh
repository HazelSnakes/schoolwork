#!/usr/bin/env bash
mkdir -p "$(dirname $1)/out"
basename=$(basename -- "$1" ".md")
echo $basename
pandoc -o "$(dirname $1)/out/$basename.pdf" --template=.format/mla-template.tex --pdf-engine=xelatex --filter=pandoc-citeproc --csl=.format/mla.csl "$1"
